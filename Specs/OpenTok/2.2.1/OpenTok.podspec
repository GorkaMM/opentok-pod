Pod::Spec.new do |s|
  s.name     = 'OpenTok'
  s.version  = '2.2.1'
  s.author   = 'TokBox'
  s.summary  = 'The OpenTok iOS SDK lets you use OpenTok video sessions in apps you build for iPad, iPhone, and iPod touch devices.'
  s.homepage = 'https://github.com/opentok/opentok-ios-sdk-webrtc/'
  s.license  = { :type => 'Apache', :file => 'LICENSE.txt' }

  s.source       = { :http => 'https://s3.amazonaws.com/artifact.tokbox.com/rel/ios-sdk/OpenTok-iOS-2.2.1.tar.bz2' }

  s.platform = :ios, '7.0'

  s.source_files = 'OpenTok.framework/Versions/A/Headers/*.h'
  s.resources    = 'OpenTok.framework/Versions/A/OpenTok'

  needed_frameworks = ['OpenTok', 'GLKit', 'CoreTelephony', 'SystemConfiguration', 'OpenGLES', 'CoreVideo', 'CoreMedia', 'CoreGraphics', 'AudioToolbox', 'AVFoundation', 'QuartzCore', 'UIKit', 'Foundation']
  needed_ldflags = '$(inherited) -framework ' + needed_frameworks.join(' -framework ')

  s.libraries = 'stdc++.6.0.9', 'pthread', 'sqlite3', 'xml2'

  s.xcconfig  = {
    'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/OpenTok/"',

    'OTHER_LDFLAGS[arch=arm64]' => needed_ldflags,
    'OTHER_LDFLAGS[arch=armv7]' => needed_ldflags,
    'OTHER_LDFLAGS[arch=armv7s]' => needed_ldflags
  }

  s.preserve_paths = 'OpenTok.framework'

  s.requires_arc = false
end