Pod::Spec.new do |s|
  s.name     = 'OpenTok'
  s.version  = '2.4.0'
  s.author   = 'TokBox'
  s.summary  = 'The OpenTok iOS SDK lets you use OpenTok video sessions in apps you build for iPad, iPhone, and iPod touch devices.'
  s.homepage = 'https://tokbox.com/opentok/libraries/client/ios/'
  s.license  = { :type => 'Apache', :file => 'LICENSE.txt' }

  s.source       = { :http => 'https://s3.amazonaws.com/artifact.tokbox.com/rel/ios-sdk/OpenTok-iOS-2.4.0.tar.bz2' }

  s.platform = :ios, '7.0'

  s.source_files = 'OpenTok.framework/Versions/A/Headers/*.h'
  s.resources    = 'OpenTok.framework/Versions/A/OpenTok'

  s.frameworks = 'OpenTok', 'VideoToolbox', 'AudioToolbox', 'AVFoundation', 'CoreAudio', 'CoreMedia', 'CoreTelephony', 'CoreVideo', 'GLKit', 'QuartzCore', 'SystemConfiguration', 'CoreGraphics', 'UIKit', 'Foundation'

  s.libraries = 'c++', 'xml2', 'sqlite3', 'pthread'

  s.xcconfig  = {
    'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/OpenTok/"'
  }

  s.preserve_paths = 'OpenTok.framework'

  s.requires_arc = false
end